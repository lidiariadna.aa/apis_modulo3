const express = require('express');
const Datastore = require('nedb');

const app = express();
app.listen(3000, () => console.log('listening at 3000'));
app.use(express.static('public'));
app.use(express.json({ limit: '1mb' }));

const database = new Datastore('database.db');
database.loadDatabase();

app.get('/api', (request, response) => {
    database.find({}, (err, data) => {
        if (err) {
            response.end();
            return;
        }
        response.json(data);   
    });
});

app.post('/api', (request, response) => {
    const data = request.body;
    const timestamp = Date.now();
    data.timestamp = timestamp;
    database.insert(data);
    response.json(data);
});

app.get('/weather:latlon', async (request, response) => {
    console.log(request, params);
    const latlon = request.params.latlon.split(',');
    console.log(latlon);
    const lat = latlon[0];
    const lon = latlon[1];
    console.log(lat,lon);
    //const aq_url = `https://api.openaq.org/v1/latest?coordinates=${lat},${lon}`;
    const api_url = `https://api.weather.gov/gridpoints/TOP/{gridX},{gridY}/forecast`;
    const fetch_response = await fetch(api_url);
    const json = await fetch_response.json();
    const forecast = await json.properties.periods[0];
    document.getElementById('weatherData').textContent = forecast.shortForecast;
    document.getElementById('temperature').textContent = forecast.temperature;
});