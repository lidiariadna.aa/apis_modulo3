function setup() {
    let lat, lon;
    const button = document.getElementById("checkin");
    button.addEventListener("click", async event => {
        const data = { lat, lon };
        const options = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
        };
        const response = await fetch("/api", options);
        const json = await response.json();
        console.log(json);
    });

    if ("geolocation" in navigator) {
        console.log('geolocation available');
        navigator.geolocation.getCurrentPosition(async position => {
            lat = position.coords.latitude;
            lon = position.coords.longitude;
            console.log(lat,lon);
            document.getElementById('latitude').textContent = lat.toFixed(2);
            document.getElementById('longitude').textContent = lon.toFixed(2);
            //const api_url = `weather/${lat},${lon}`;
        	const api_url = `https://api.weather.gov/gridpoints/TOP/31,80/forecast`;
            const response = await fetch(api_url);
            const json = await response.json();
            const forecast = await json.properties.periods[0];
            document.getElementById('weatherData').textContent = forecast.shortForecast;
            document.getElementById('temperature').textContent = forecast.temperature;
        });
    } else {
        console.log('geolocation not available');
    }
}